const functions = require('firebase-functions');
const Rx = require('rxjs');

// Saves a message to the Firebase Realtime Database but sanitizes the text by removing swearwords.
exports.addMessage = functions.https.onCall((data, context) => {
    console.log(data);
    console.log(Rx.Observable);
    Rx.Observable.interval(1000).subscribe(val => {
        console.log(val);
        if (val === 5) {
            return {
                firstNumber: 'firstNumber',
                secondNumber: 'secondNumber',
                operator: '+',
                operationResult: 'firstNumber + secondNumber'
            };
        }
    });
});